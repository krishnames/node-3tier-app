# A security group for the ELB so it is accessible via the web
resource "aws_security_group" "toptal_api_elb_sg" {
  name = "toptal-api-elb-sg"
  description = "Toptal security group for api elb"
  vpc_id      = "${aws_vpc.toptal.id}"

  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS access from anywhere
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Api ELB SG"
  }
}




resource "aws_security_group" "toptal_api_sg" {
  name        = "toptal-api-sg"
  description = "Toptal security group"
  vpc_id      = "${aws_vpc.toptal.id}"
  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups  = ["${aws_security_group.toptal_api_elb_sg.id}"]
  }

  # HTTPS access from anywhere
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    security_groups  = ["${aws_security_group.toptal_api_elb_sg.id}"]
  }


  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = ["${aws_security_group.toptal_bastion_sg.id}"]
  }
  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Api Ec2 SG"
  }

}

data "template_file" "api_script" {
  template = "${file("scripts/api_script.tpl")}"
  vars = {
    toptal_region = "${var.region}"
    db_elb_dns_name = "${aws_db_instance.toptal_db_master.address}",
    api_port = "${var.api_port}",
    db_user = "${var.db_user}",
    db_password = "${trimspace(file("secrets/db-password.txt"))}",
    api_db = "${var.api_db}",
    git_repo_url = "${var.git_repo_url}"
  }
  depends_on = ["aws_db_instance.toptal_db_master"]
}

## Creating API Launch Configuration
resource "aws_launch_configuration" "toptal_api_lc" {
  name_prefix = "api-"
  image_id  = "${var.toptal_api_ami}"
  instance_type = "${var.ec2_instance_type}"
  iam_instance_profile = "${aws_iam_instance_profile.topal_iam_ec2_profile.name}"
  security_groups = ["${aws_security_group.toptal_api_sg.id}"]
  key_name = "${aws_key_pair.auth.id}"
  user_data = "${data.template_file.api_script.rendered}"
  lifecycle {
    create_before_destroy = true
  }
}

## Creating Api AutoScaling Group
resource "aws_autoscaling_group" "toptal_api_asg" {
  launch_configuration = "${aws_launch_configuration.toptal_api_lc.id}"
  name = "${aws_launch_configuration.toptal_api_lc.name}-api"
  vpc_zone_identifier = ["${aws_subnet.toptal_private_1.id}","${aws_subnet.toptal_private_2.id}"]
  min_size = 2
  max_size = 2
  load_balancers = ["${aws_elb.toptal_api_elb.name}"]
  health_check_type = "ELB"
  depends_on = ["aws_route_table_association.toptal_private_nat_a","aws_db_instance.toptal_db_master"]
  force_delete = true
  health_check_grace_period = 300
  lifecycle {
    create_before_destroy = true
  }
  tags = [
    {
      "key" = "Name"
      "value" = "api-${var.api_version}"
      "propagate_at_launch" = true
    },
  ]

}
#scale up alarm
resource "aws_autoscaling_policy" "toptal_api_cpu_policy" {
  name = "toptal-api-cpu-policy"
  autoscaling_group_name = "${aws_autoscaling_group.toptal_api_asg.name}"
  adjustment_type = "ChangeInCapacity"
  scaling_adjustment = "1"
  cooldown = "300"
  policy_type = "SimpleScaling"
}


resource "aws_cloudwatch_metric_alarm" "toptal_api_cpu_alarm" {
  alarm_name = "toptal-api-cpu-alarm"
  alarm_description = "toptal-api-cpu-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "60"
  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.toptal_api_asg.name}"
  }
  actions_enabled = true
  alarm_actions = ["${aws_autoscaling_policy.toptal_api_cpu_policy.arn}"]
}

# scale down alarm
resource "aws_autoscaling_policy" "toptal_api_cpu_policy_scaledown" {
  name = "toptal-api-cpu-policy-scaledown"
  autoscaling_group_name = "${aws_autoscaling_group.toptal_api_asg.name}"
  adjustment_type = "ChangeInCapacity"
  scaling_adjustment = "-1"
  cooldown = "300"
  policy_type = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "toptal_api_cpu_alarm_scaledown" {
  alarm_name = "toptal-api-cpu-alarm-scaledown"
  alarm_description = "toptal-api-cpu-alarm-scaledown"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "10"
  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.toptal_api_asg.name}"
  }
  actions_enabled = true
  alarm_actions = ["${aws_autoscaling_policy.toptal_api_cpu_policy_scaledown.arn}"]
}

### Creating API ELB
resource "aws_elb" "toptal_api_elb" {
  name = "toptal-api-elb"
  security_groups = ["${aws_security_group.toptal_api_elb_sg.id}"]
  subnets  = ["${aws_subnet.toptal_public_1.id}","${aws_subnet.toptal_public_2.id}"]

  cross_zone_load_balancing = true
  connection_draining = true
  connection_draining_timeout = 400

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 10
    interval = 30
    target = "HTTP:${var.api_port}/api/status"
  }
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "${var.api_port}"
    instance_protocol = "http"
  }
}

//Add Root Route53 Records
resource "aws_route53_record" "toptal_api_dns_record" {
  zone_id = "${var.hosted_zone_id}"
  name = "${var.api_url}"
  type = "CNAME"
  ttl  = "${var.toptal_dns_ttl}"
  records = ["${aws_elb.toptal_api_elb.dns_name}"]
  depends_on = ["aws_autoscaling_group.toptal_api_asg"]

}

