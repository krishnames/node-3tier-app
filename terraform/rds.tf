resource "aws_db_subnet_group" "toptal_subnet_group" {
  name        = "toptal_subnet_group"
  description = "Our main group of subnets"
  subnet_ids  = ["${aws_subnet.toptal_public_1.id}","${aws_subnet.toptal_public_2.id}"]
}

resource "aws_security_group" "toptal_db_sg" {
  name = "toptal_db_sg"

  description = "RDS postgres sg "
  vpc_id = "${aws_vpc.toptal.id}"

  # Only postgres in
  ingress {
    from_port = 5432
    to_port = 5432
    protocol = "tcp"
    security_groups  = ["${aws_security_group.toptal_api_sg.id}"]
  }

  # Allow all outbound traffic.
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "RDS SG"
  }

}

resource "aws_db_instance" "toptal_db_master" {
  allocated_storage        = 20 # gigabytes
  backup_retention_period  = 5   # in days
  final_snapshot_identifier = "${var.api_db}-${md5(timestamp())}"
  db_subnet_group_name     = "${aws_db_subnet_group.toptal_subnet_group.id}"
  engine                   = "postgres"
  engine_version           = "9.6.14"
  identifier               = "toptal-db-master"
  instance_class           = "${var.rds_instance_type}"
  multi_az                 = "true"
  name                     = "${var.api_db}"
  password                 = "${trimspace(file("secrets/db-password.txt"))}"
  port                     = 5432
  maintenance_window 	   = "Mon:00:00-Mon:03:00"
  backup_window      	   = "03:00-06:00"
  publicly_accessible      = false
  storage_encrypted        = false # you should always do this
  storage_type             = "gp2"
  username                 = "${var.db_user}"
  vpc_security_group_ids   = ["${aws_security_group.toptal_db_sg.id}"]
  monitoring_interval  = "30"
  monitoring_role_arn  = aws_iam_role.rds_enhanced_monitoring.arn


}

#Replicate db

resource "aws_db_instance" "toptal_db_replica" {
  replicate_source_db = "${aws_db_instance.toptal_db_master.id}"
  engine                   = "postgres"
  engine_version           = "9.6.14"
  identifier               = "toptaldb-read-replica"
  instance_class           = "${var.rds_instance_type}"
  allocated_storage        = 20 # gigabytes
  skip_final_snapshot      = true
  final_snapshot_identifier = null
# Username and password must not be set for replicas
  username = ""
  password = ""
  port                     = 5432

  vpc_security_group_ids   = ["${aws_security_group.toptal_db_sg.id}"]
  maintenance_window = "Tue:00:00-Tue:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = 0

}

//Add Root Route53 Records
resource "aws_route53_record" "toptal_db_dns_record" {
  zone_id = "${var.hosted_zone_id}"
  name = "${var.db_url}"
  type = "CNAME"
  ttl  = "${var.toptal_dns_ttl}"
  records = ["${aws_db_instance.toptal_db_master.address}"]

  depends_on = ["aws_db_instance.toptal_db_master"]
}

##################################################
# Create an IAM role to allow enhanced monitoring
##################################################
resource "aws_iam_role" "rds_enhanced_monitoring" {
  name_prefix        = "rds-enhanced-monitoring-"
  assume_role_policy = data.aws_iam_policy_document.rds_enhanced_monitoring.json
}

resource "aws_iam_role_policy_attachment" "rds_enhanced_monitoring" {
  role       = aws_iam_role.rds_enhanced_monitoring.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

data "aws_iam_policy_document" "rds_enhanced_monitoring" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["monitoring.rds.amazonaws.com"]
    }
  }
}

