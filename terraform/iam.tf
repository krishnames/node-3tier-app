resource "aws_iam_role" "toptal_iam_role" {
  name = "toptal-iam-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  tags = {
      tag-key = "tag-value"
  }
}

resource "aws_iam_instance_profile" "topal_iam_ec2_profile" {
  name = "toptal-iam-ec2-profile"
  role = "${aws_iam_role.toptal_iam_role.name}"
}

resource "aws_iam_role_policy" "toptal_ec2_log_policy" {
  name = "toptal_ec2_log_policy"
  role = "${aws_iam_role.toptal_iam_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::toptal-log-bucket",
        "arn:aws:s3:::toptal-log-bucket/*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "toptal_cloudWatch_server_role_policy" {
  name = "toptal-cloudWatch-server-role-Policy"
  role = "${aws_iam_role.toptal_iam_role.id}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "cloudwatch:PutMetricData",
                "ec2:DescribeTags",
                "logs:PutLogEvents",
                "logs:DescribeLogStreams",
                "logs:DescribeLogGroups",
                "logs:CreateLogStream",
                "logs:CreateLogGroup"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ssm:GetParameter"
            ],
            "Resource": "arn:aws:ssm:*:*:parameter/AmazonCloudWatch-*"
        }
    ]
}
EOF
}


