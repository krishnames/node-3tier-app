# A security group for the ELB so it is accessible via the web
resource "aws_security_group" "toptal_web_elb_sg" {
  name = "toptal-web-elb-sg"
  description = "Toptal security group for web elb"
  vpc_id      = "${aws_vpc.toptal.id}"

  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS access from anywhere
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Web ELB SG"
  }


}

# A security group for the ELB so it is accessible via the web
resource "aws_security_group" "toptal_web_sg" {
  name        = "toptal-web-sg"
  description = "Toptal security group"
  vpc_id      = "${aws_vpc.toptal.id}"

  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups  = ["${aws_security_group.toptal_web_elb_sg.id}"]
  }
  # HTTPS access from anywhere
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    security_groups  = ["${aws_security_group.toptal_web_elb_sg.id}"]
  }

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = ["${aws_security_group.toptal_bastion_sg.id}"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Web Ec2 SG"
  }
}

## Creating Web Launch Configuration
resource "aws_launch_configuration" "toptal_web_lc" {
  name_prefix = "web-"
  image_id               = "${var.toptal_web_ami}"
  instance_type          = "${var.ec2_instance_type}"
  iam_instance_profile = "${aws_iam_instance_profile.topal_iam_ec2_profile.name}"
  security_groups        = ["${aws_security_group.toptal_web_sg.id}"]
  key_name = "${aws_key_pair.auth.id}"
  user_data = "${data.template_file.web_script.rendered}"
  lifecycle {
    create_before_destroy = true
  }
}

data "template_file" "web_script" {
  template = "${file("scripts/web_script.tpl")}"
  vars = {
    toptal_region = "${var.region}"
    api_elb_dns_name = "${aws_elb.toptal_api_elb.dns_name}"
    web_port =  "${var.web_port}",
    git_repo_url = "${var.git_repo_url}"
  }
  depends_on = ["aws_autoscaling_group.toptal_api_asg"]
}

## Creating Web AutoScaling Group
resource "aws_autoscaling_group" "toptal_web_asg" {
  launch_configuration = "${aws_launch_configuration.toptal_web_lc.id}"
  name = "${aws_launch_configuration.toptal_web_lc.name}-asg"
  vpc_zone_identifier = ["${aws_subnet.toptal_private_1.id}","${aws_subnet.toptal_private_2.id}"]
  min_size = 2
  max_size = 2
  load_balancers = ["${aws_elb.toptal_web_elb.name}"]
  health_check_type = "ELB"
  depends_on = ["aws_autoscaling_group.toptal_api_asg"]
  health_check_grace_period = 300
  force_delete = true
  lifecycle {
    create_before_destroy = true
  }
  tags = [
    {
      "key" = "Name"
      "value" = "web-${var.web_version}"
      "propagate_at_launch" = true
    },
  ]

}

# scale up alarm
resource "aws_autoscaling_policy" "toptal_web_cpu_policy" {
  name = "toptal-web-cpu-policy"
  autoscaling_group_name = "${aws_autoscaling_group.toptal_web_asg.name}"
  adjustment_type = "ChangeInCapacity"
  scaling_adjustment = "1"
  cooldown = "300"
  policy_type = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "toptal_web_cpu_alarm" {
  alarm_name = "toptal-web-cpu-alarm"
  alarm_description = "toptal-web-cpu-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "60"
  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.toptal_web_asg.name}"
  }
  actions_enabled = true
  alarm_actions = ["${aws_autoscaling_policy.toptal_web_cpu_policy.arn}"]
}



# scale down alarm
resource "aws_autoscaling_policy" "toptal_web_cpu_policy_scaledown" {
  name = "toptal-web-cpu-policy-scaledown"
  autoscaling_group_name = "${aws_autoscaling_group.toptal_web_asg.name}"
  adjustment_type = "ChangeInCapacity"
  scaling_adjustment = "-1"
  cooldown = "300"
  policy_type = "SimpleScaling"
}


resource "aws_cloudwatch_metric_alarm" "toptal_web_cpu_alarm_scaledown" {
  alarm_name = "toptal-web-cpu-alarm-scaledown"
  alarm_description = "toptal-web-cpu-alarm-scaledown"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "10"
  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.toptal_web_asg.name}"
  }
  actions_enabled = true
  alarm_actions = ["${aws_autoscaling_policy.toptal_web_cpu_policy_scaledown.arn}"]
}


### Creating ELB
resource "aws_elb" "toptal_web_elb" {
  name = "toptal-web-elb"
  security_groups = ["${aws_security_group.toptal_web_elb_sg.id}"]
  subnets  = ["${aws_subnet.toptal_public_1.id}","${aws_subnet.toptal_public_2.id}"]
  cross_zone_load_balancing = true
  connection_draining = true
  connection_draining_timeout = 400
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 10
    interval = 30
    target = "HTTP:${var.web_port}/"
  }
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "${var.web_port}"
    instance_protocol = "http"
  }
}

//Add Root Route53 Records
resource "aws_route53_record" "toptal_web_dns_record" {
  zone_id = "${var.hosted_zone_id}"
  name = "${var.web_url}"
  type = "CNAME"
  ttl  = "${var.toptal_dns_ttl}"
  records = ["${aws_elb.toptal_web_elb.dns_name}"]

  depends_on = ["aws_autoscaling_group.toptal_web_asg"]
  
}

