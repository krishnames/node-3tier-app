output "web_elb_dns_name" {
  value = "${aws_elb.toptal_web_elb.dns_name}"
}
output "api_elb_dns_name" {
  value = "${aws_elb.toptal_api_elb.dns_name}"
}
output "master_db_elb_dns_name" {
  value = "${aws_db_instance.toptal_db_master.address}"
}
output "replica_db_elb_dns_name" {
  value = "${aws_db_instance.toptal_db_replica.address}"
}
output "web_url" {
  value = "${var.web_url}"
}
output "api_url" {
  value = "${var.api_url}"
}
output "db_url" {
  value = "${var.db_url}"
}
output "cloudfront_dns_name" {
  value = "${aws_cloudfront_distribution.toptal_cloudfront_distribution.domain_name}"
}


