// Setup Bucket
resource "aws_s3_bucket" "toptal_cdn_bucket" {
  bucket = "${var.toptal_cdn_bucket}"
  acl = "private"
  tags =  {
    Name = "${var.toptal_cdn_bucket}"
  }
policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "OnlyCloudfrontReadAccess",
      "Effect": "Allow",
      "Principal": {
         "AWS": "${aws_cloudfront_origin_access_identity.toptal_origin_access_identity.iam_arn}"
      },
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${var.toptal_cdn_bucket}/*"
    }
  ]
}
POLICY
}

resource "aws_cloudfront_origin_access_identity" "toptal_origin_access_identity" {
  comment = "toptal"
}

resource "aws_cloudfront_distribution" "toptal_cloudfront_distribution" {
  origin {
    domain_name = "${aws_s3_bucket.toptal_cdn_bucket.bucket_domain_name}"
    origin_id = "S3-${var.toptal_cdn_bucket}"
    s3_origin_config  {
      origin_access_identity = "${aws_cloudfront_origin_access_identity.toptal_origin_access_identity.cloudfront_access_identity_path}"
    }
  }

  enabled = true
  price_class = "${var.toptal_cdn_price_class}"
  default_cache_behavior {
    allowed_methods = [ "DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT" ]
    cached_methods = [ "GET", "HEAD" ]
    target_origin_id = "S3-${var.toptal_cdn_bucket}"
    forwarded_values {
      query_string = true
      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "allow-all"
    min_ttl = 0
    default_ttl = 3600
    max_ttl = 86400
  }
  retain_on_delete = "${var.toptal_cdn_retain_on_delete}"
  viewer_certificate {
    cloudfront_default_certificate = true
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}

