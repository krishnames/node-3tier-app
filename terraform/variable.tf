variable "num" {
    default = 1
  }
variable "region" {
  description = "AWS region for hosting our your network"
  default = "us-west-2"
}
variable "ec2_instance_type"  {
  description = "AWS EC2 Instance type"
  default = "t2.micro"
}
variable "rds_instance_type"  {
  description = "AWS RDS Instance type"
  default = "db.t2.micro"
}

variable "public_key_path" {
  description = "Enter the path to the SSH Public Key to add to AWS."
  default = "ssh/toptal.pub"
}
variable "private_key_path" {
  description = "Enter the path to the SSH private Key to add to AWS."
  default = "ssh/toptal"
}

variable "git_repo_url" {
  description = "URL to the public git repo"
  default = "https://gitlab.com/krishnames/node-3tier-app.git"
}

variable "key_name" {
  description = "Key name for SSHing into EC2"
  default = "toptal"
}
variable "amis" {
  default = {
#   us-west-2 = "ami-0b37e9efc396e4c38"   #Ubuntu 16.04 LTS
#   us-west-2 = "ami-00f7c900d2e7133e1"   # Centos 7.5
    us-west-2 = "ami-082b5a644766e0e6f"
  }
}
variable "api_db" {
    description = "API DB name"
    default = "apidb"
}
variable "db_user" {
    description = "API DB user name"
    default = "api"
}
variable "api_port" {
    description = "API Port"
    default = 80
}

variable "web_port" {
    description = "Web Port"
    default = 80
}
variable "web_version" {
    description = "Version of web to be deployed"
    default = "ver1"
}
variable "api_version" {
    description = "Version of API to be deployed"
    default = "ver1"
}

variable "api_url" {
    description = "Version of API to be deployed"
    default = "api.krishnaslife.com"
}

variable "web_url" {
  description = "Name of the domain where record(s) need to create"
  default = "toptal.krishnaslife.com"
}

variable "db_url" {
  description = "Name of the domain where record(s) need to create"
  default = "db.krishnaslife.com"
}


variable hosted_zone_id {
  description = "ID for the domain hosted zone"
  default = "Z3P2XOV0LAB4JW"
}
variable alias_zone_id {
        description = "Fixed hardcoded constant zone_id"
        default = "Z2FDTNDATAQYW2"
}
variable "toptal_dns_ttl" {
  default = "3600"
}



#CDN

variable "toptal_cdn_bucket" {
  default = "toptal-cdn-bucket"
}

variable "toptal_cdn_retain_on_delete" {
        description = "Instruct CloudFront to simply disable the distribution instead of delete"
        default = false
}

variable "toptal_cdn_price_class" {
        description = "Price classes provide you an option to lower the prices you pay to deliver content out of Amazon CloudFront"
        default = "PriceClass_All"
}


