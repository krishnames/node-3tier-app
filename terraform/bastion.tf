# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "toptal_bastion_sg" {
  name        = "toptal-bastion-sg"
  description = "toptal bastion sg"
  vpc_id      = "${aws_vpc.toptal.id}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Bastion SG"
  }

}


#Bastion host to access servers as proxy
resource "aws_instance" "bastion" {
  availability_zone = "us-west-2a"
  instance_type = "${var.ec2_instance_type}"
  ami = "${lookup(var.amis, var.region)}"
  key_name = "${aws_key_pair.auth.id}"
  vpc_security_group_ids = ["${aws_security_group.toptal_bastion_sg.id}"]
  subnet_id = "${aws_subnet.toptal_public_1.id}"
  tags = {
    Name = "Bastion Host"
  }
}

