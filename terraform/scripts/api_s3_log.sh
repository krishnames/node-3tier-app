#!/bin/bash
instance_id=`ec2-metadata -i| awk '{ print $2 }'`
current_time=`date "+%Y.%m.%d-%H.%M.%S"`

aws s3 cp /var/log/messages s3://toptal-log-bucket/api/$instance_id/messages-$current_time > /dev/null 2>&1
> /var/log/messages

aws s3 cp /var/log/api-stdout.log s3://toptal-log-bucket/api/$instance_id/api-stdout.log-$current_time > /dev/null 2>&1
> /var/log/api-stdout.log

aws s3 cp /var/log/api-stderr.log s3://toptal-log-bucket/api/$instance_id/api-stdout.log-$current_time > /dev/null 2>&1
> /var/log/api-stderr.log

aws s3 cp /var/log/supervisor/supervisord.log s3://toptal-log-bucket/api/$instance_id/supervisord.log-$current_time > /dev/null 2>&1
>/var/log/supervisor/supervisord.log

