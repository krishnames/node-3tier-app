#This script is running from jenkins execute shell

cd terraform
TOPTAL_API_AMI=`packer.io build -machine-readable packer-api-ami.json | awk -F, '$0 ~/artifact,0,id/ {print $6}' | cut -d: -f2`
echo 'variable "toptal_api_ami" { default = "'$TOPTAL_API_AMI'" }' > apiami.tf
