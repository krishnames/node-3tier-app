#!/bin/bash

#Change default awslogs region
sed -i 's/us-east-1/${toptal_region}/g' /etc/awslogs/awscli.conf

#Add supervisor and web logs to awslogs conf
echo "[/var/log/web-stdout.log]
file = /var/log/web-stdout.log
buffer_duration = 5000
log_stream_name = {instance_id}
initial_position = start_of_file
log_group_name = /var/log/web-stdout.log

[/var/log/web-stderr.log]
file = /var/log/web-stderr.log
buffer_duration = 5000
log_stream_name = {instance_id}
initial_position = start_of_file
log_group_name = /var/log/web-stderr.log

[/var/log/supervisor/supervisord.log]
datetime_format = %b %d %H:%M:%S
file = /var/log/supervisor/supervisord.log
buffer_duration = 5000
log_stream_name = {instance_id}
initial_position = start_of_file
log_group_name = /var/log/supervisor/supervisord.log" | sudo tee -a /etc/awslogs/awslogs.conf >/dev/null
sudo systemctl restart awslogsd.service


#supervisord conf for web
echo "[program:web]
command=/bin/npm start
directory=/var/www/html/web
autostart=true
autorestart=true
stderr_logfile=/var/log/web-stderr.log
stdout_logfile=/var/log/web-stdout.log
startretries=3
environment=PORT=${web_port},API_HOST='http://${api_elb_dns_name}'"| sudo tee /etc/supervisord.d/web.ini >/dev/null
sudo systemctl restart supervisord


until sudo supervisorctl status web | grep RUNNING > /dev/null ; do sleep 5; done
