#This script is running from jenkins execute shell 

cd terraform
TOPTAL_WEB_AMI=`packer.io build -machine-readable packer-web-ami.json | awk -F, '$0 ~/artifact,0,id/ {print $6}' | cut -d: -f2`
echo 'variable "toptal_web_ami" { default = "'$TOPTAL_WEB_AMI'" }' > webami.tf
