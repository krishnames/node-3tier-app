#!/bin/bash

#Change default awslogs region
sed -i 's/us-east-1/${toptal_region}/g' /etc/awslogs/awscli.conf


#Add supervisor and web logs to awslogs conf
echo "[/var/log/api-stdout.log]
file = /var/log/api-stdout.log
buffer_duration = 5000
log_stream_name = {instance_id}
initial_position = start_of_file
log_group_name = /var/log/api-stdout.log

[/var/log/api-stderr.log]
file = /var/log/api-stderr.log
buffer_duration = 5000
log_stream_name = {instance_id}
initial_position = start_of_file
log_group_name = /var/log/api-stderr.log

[/var/log/supervisor/supervisord.log]
datetime_format = %b %d %H:%M:%S
file = /var/log/supervisor/supervisord.log
buffer_duration = 5000
log_stream_name = {instance_id}
initial_position = start_of_file
log_group_name = /var/log/supervisor/supervisord.log" | sudo tee -a /etc/awslogs/awslogs.conf >/dev/null
sudo systemctl restart awslogsd.service


#supervisord conf for web
echo "[program:api]
command=/bin/npm start
directory=/var/www/html/api
autostart=true
autorestart=true
startretries=3
stderr_logfile=/var/log/api-stderr.log
stdout_logfile=/var/log/api-stdout.log
environment=PORT=${api_port},DB='postgres://${db_user}:${db_password}@${db_elb_dns_name}/${api_db}'"| sudo tee /etc/supervisord.d/api.ini >/dev/null
sudo systemctl restart supervisord

#
until sudo supervisorctl status api | grep RUNNING > /dev/null ; do sleep 5; done

