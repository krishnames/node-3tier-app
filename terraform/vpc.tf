# Create a VPC to launch our instances into
resource "aws_vpc" "toptal" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Name = "toptal_vpc"
  }
}

# Public subnet in the first AZ
resource "aws_subnet" "toptal_public_1" {
  vpc_id = "${aws_vpc.toptal.id}"
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone = "us-west-2a"
  tags = {
    Name = "Pulic subnet - AZ1"
  }
}

# Public subnet in the second AZ
resource "aws_subnet" "toptal_public_2" {
  vpc_id = "${aws_vpc.toptal.id}"
  cidr_block = "10.0.2.0/24"
  map_public_ip_on_launch = "true"
  availability_zone = "us-west-2b"
  tags = {
    Name = "Pulic subnet - AZ2"
  }
}

#Private subnet in the first AZ
resource "aws_subnet" "toptal_private_1" {
  vpc_id = "${aws_vpc.toptal.id}"
  cidr_block = "10.0.4.0/24"
  map_public_ip_on_launch = "false"
  availability_zone = "us-west-2a"
  tags = {
    Name = "Private subnet - AZ1"
  }
}
#Private subnet in the second AZ
resource "aws_subnet" "toptal_private_2" {
  vpc_id = "${aws_vpc.toptal.id}"
  cidr_block = "10.0.5.0/24"
  map_public_ip_on_launch = "false"
  availability_zone = "us-west-2b"
  tags = {
    Name = "Private subnet - AZ2"
  }
}


# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "toptal_igw" {
  vpc_id = "${aws_vpc.toptal.id}"
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.toptal.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.toptal_igw.id}"
}

#NAT gateway
resource "aws_nat_gateway" "toptal_nat_gw" {
  allocation_id = "${aws_eip.toptal_nat.id}"
  subnet_id = "${aws_subnet.toptal_public_1.id}"
  depends_on = ["aws_internet_gateway.toptal_igw"]
}

# NAT Routing table
resource "aws_route_table" "toptal_private_nat" {
  vpc_id = "${aws_vpc.toptal.id}"
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.toptal_nat_gw.id}"
  }
  tags = {
     Name = "Toptal Private NAT"
  }
}


# private routes
resource "aws_route_table_association" "toptal_private_nat_a" {
  subnet_id = "${aws_subnet.toptal_private_1.id}"
  route_table_id = "${aws_route_table.toptal_private_nat.id}"
}

resource "aws_route_table_association" "toptal_private_nat_b" {
  subnet_id = "${aws_subnet.toptal_private_2.id}"
  route_table_id = "${aws_route_table.toptal_private_nat.id}"
}

