resource "aws_s3_bucket" "toptal_log_bucket" {
  bucket = "toptal-log-bucket"
  acl = "private"
  tags = {
    Name = "toptal-log-bucket"
  }
}

resource "aws_s3_bucket_object" "toptal_web_log_dir" {
  bucket = "${aws_s3_bucket.toptal_log_bucket.id}"
  acl    = "private"
  key    = "web/"
  source = "/dev/null"
}

resource "aws_s3_bucket_object" "toptal_api_log_dir" {
  bucket = "${aws_s3_bucket.toptal_log_bucket.id}"
  acl    = "private"
  key    = "api/"
  source = "/dev/null"
}
